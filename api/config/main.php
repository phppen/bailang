<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\modules\v1\controllers',
    'bootstrap' => ['log'],
    // 必须指定 modules
    'modules' => [
        'v1' => [
            'class' => 'api\modules\v1\Module',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-api',
            // 为了使 API 接收 JSON 格式的输入数据
            // 配置 request 应用程序组件的 parsers 属性
            // 使用 yii\web\JsonParser 用于JSON输入
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        // 定义Header 输出为 application/json
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {

                $response = $event->sender;
                $code = $response->getStatusCode();
                $msg = $response->statusText;
                if ($code == 404) {
                    !empty($response->data['message']) && $msg = $response->data['message'];
                }
                // 手动将抛出异常的状态码改为
                switch ($msg) {
                    case '用户名不能为空。': $code = '101';break;
                    case '用户名或密码错误.': $code = '102';break;
                    default : $code = $code;break;
                }
                $data = [
                    'code' => $code,
                    'msg' => "Oh No! See: ".$msg,
                ];
                $code == 200 && $data['data'] = $response->data;
                $response->data = $data;
                $response->format = yii\web\Response::FORMAT_JSON;
            },
        ],
        // 用户权限设置
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => null,
            'enableSession' => false
            // 'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the api
            'name' => 'advanced-api',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        // yii错误处理组件
        //'errorHandler' => [
            //'errorAction' => 'site/error',
        //],
        // Url美化
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' =>true,
            'rules' => require(__DIR__ . '/url-rules.php'),
        ],
    ],
    'params' => $params,
];
