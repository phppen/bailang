<?php
return [
    'adminEmail' => 'admin@example.com',
    'user.apiTokenExpire' => 1*24*3600, // token 有效期默认1天，可以按照自己的项目需求配置
];