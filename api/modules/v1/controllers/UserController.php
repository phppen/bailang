<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\filters\auth\HttpBearerAuth;
use common\models\User;
use api\models\LoginForm;
use yii\web\IdentityInterface;

class UserController extends ActiveController
{
    /**
     * 为控制器指定对应模型
     * 通过指定 yii\rest\ActiveController::modelClass
     * 作为 api\modules\v1\models\User，
     * 控制器就能知道使用哪个模型去获取和处理数据。
     * 当然这个api\modules\v1\models\User
     * 是可以直接继承\yii\db\ActiveRecord的
     */
    public $modelClass = 'api\models\User';

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [

            'authenticator' => [
                // Yii2 官方d的 HttpBearerAuth 的 restful 验证方式
                'class' => HttpBearerAuth::className(),
                'optional' => [
                    'index',
                    'login',
                    'signup-test',
                ],
            ]
        ]);
    }

    /**
     * 添加测试用户
     */
    public function actionSignupTest ()
    {
        $user = new User();
        $user->generateAuthKey();
        $user->setPassword('123456');
        $user->username = '888';
        $user->email = '888@888.com';

        return $user->save(false);
    }

    /**
     * 登录
     */
    public function actionLogin ()
    {
        $model = new LoginForm;
        $model->setAttributes(Yii::$app->request->post());

        if ($user = $model->login()) {
            return $user->api_token;
        } else {
            return $model->errors;
        }
    }

    /**
     * 获取用户信息
     */
    public function actionUserProfile ()
    {
        // 到这一步，token都认为是有效的了
        // HttpBearerAuth 已通过行为绑定到本类，所以可以直接调用 HttpBearerAuth 中的 authenticate 方法
        $user = $this->authenticate(Yii::$app->user, Yii::$app->request, Yii::$app->response);
        // Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
        return [
            'id' => $user->id,
            'username' => $user->username,
            'email' => $user->email,
        ];
    }
}
