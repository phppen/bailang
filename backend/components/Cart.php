<?php
namespace backend\components;

class Cart
{
    // 商品价格
    const PRICE_BUTTER = 1.00;
    const PRICE_MILK = 3.00;
    const PRICE_EGGS = 6.95;

    // 保存商品信息
    protected $products = array();

    public function add($product, $quantity)
    {
        $this->products[$product] = $quantity;
    }

    // 通过商品名称获取商品数量
    public function getQuantity($product)
    {
        return isset($this->products[$product]) ? $this->products[$product] : FALSE;
    }

    // 获取总的商品价格
    public function getTotal($tax)
    {
        $total = 0.00;

        /**
         * 回调函数
         */
        $callback = function ($quantity, $product) use ($tax, &$total)
        {
            // echo "info::Quantity:" . $quantity."- Product:".$product."<br/>";
            /**
             * 获取定义的常量关于商品的单价
             * constant 返回一个常量的值
             * __CLASS__返回当前类的名称：backend\components\Cart
             * 这段代码的原形就是：constant("backend\\components\\Cart::PRICE_MILK");
             */
            //$pricePerItem = constant("backend\\components\\Cart::PRICE_" . strtoupper($product));
            // 获取商品的单价
            $pricePerItem = constant(__CLASS__ . "::PRICE_" . strtoupper($product));
            // 计算总价格(单价 * 数量 * 税额)
            $total += ($pricePerItem * $quantity) * ($tax + 1.0);
        };

        /**
         *   [array_walk] 使用用户自定义函数对数组中的每个元素做回调处理
         *   ---------------------------------------------------
         *   这里利用 array_walk 为 callback 函数对 $products 素组中每个元素做回调处理
         *   ---------------------------------------------------
         *   注意：函数和数组元素的对应是反的，也就是函数 $value 对应数据的 key
         *   ===================================================
         *   $callback = function ($value,$key)
         *   {
         *      echo "The key $key has the value $value<br>";
         *   }
         *   $products = ["1"=>"butter","3"=>"milk","6"=>"eggs"];
         *   array_walk($a,$callback);
         *   ---------------------------------------------------
         *   ----------------------- 输出 -----------------------
         *   The key 1 has the value butter
         *   The key 3 has the value milk
         *   The key 6 has the value eggs
         *   ===================================================
         */
        array_walk($this->products, $callback);
        // 对最终金额作四舍五入取整操作，并保留2位小数
        return round($total, 2);
    }
}