<?php
namespace backend\components;

class User
{
    private $_emailSenderObject;

    public function __construct(EmailSenderBy163 $emailSenderObject)
    {
        $this->_emailSenderObject = $emailSenderObject;
    }
    public function lastSend()
    {
        if ($this->_emailSenderObject->send()) {
            return true;
        }
    }
}
?>