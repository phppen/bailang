<?php
namespace backend\components;

class Test
{
    public $name;
    public $age;
    private $class;

    public $_definitions = [];

    public function __get($name)
    {
        return isset($_definitions[$name]) ? $this->_definitions[$name] : null;
    }


    public function __set($name, $value)
    {
        $this->_definitions[$name] = $value;
    }

    public static function className()
    {
        // 获取静态绑定后的类名
        return get_called_class();
    }
}