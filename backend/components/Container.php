<?php
namespace backend\components;

use Yii;
use PHPUnit\Framework\Exception;

Class Container
{
    public function get($class, $params = [])
    {
        return $this->build($class, $params);
    }

    /**
     * 该方法的作用，主要是通过获取构造函数(__construct)里指定参数所指定形参对象类型(指定的类)来获得完整的类名并实例化
     *
     * ① 通过反射获得该类（例如:User）并实例化类
     * ② 获得构造方法的信息
     * ③ 获得构造方法所有的参数信息
     * ④ 判断并获得参数指定的类的信息
     * ⑤ 根据上一步获得的类的信息取得完整的类名(string)，例如（'backend\components\EmailSenderBy163'）
     * ⑥ 调用 get() 方法进行递归
     * ⑦ 通过ReflectionClass的newInstanceArgs()方法根据给定的参数创建一个新的类实例并返回最终的输出

     * @param $class 类名的指定，例如：'backend\components\EmailSenderBy163'
     * @param $params 参数
     * @return object
     */
    public function build($class, $params)
    {
        // 记录参数，和参数类型
        $dependencies = [];

        // 一、通过反射获得该类
        $reflection = new \ReflectionClass($class);

        // 二、获得构造函数的信息，得到 ReflectionMethod 对象
        $constructor = $reflection->getConstructor();

        if ($constructor !== null) {

            // 三、通过 ReflectionMethod 对象的 getParameters()方法获得构造方法的所有参数，并返回 ReflectionParameter 对象
            foreach ($constructor->getParameters() as $param) {

                // 四、通过 ReflectionParameter 对象的 getClass()方法获取参数指定的class类，并返回 ReflectionClass 对象
                $paramClass = $param->getClass();

                // 判断是否获取到该参数返回的类是否指定
                // 也就是 __construct(EmailSenderBy163 $emailSenderObject) 也就是该参数是否有指定类
                if ($paramClass !== null) {
                    // 五、通过ReflectionClass类的getName()方法获得参数指定的类的名称（例如：backend\components\EmailSenderBy163）
                    $paramClassName = $paramClass->getName();
                    // 六、调用自身的get()方法对该类进行递归,并将数组结果保存到 $dependencies 并进行实例化
                    $dependencies[] = $this->get($paramClassName, $params);
                    /**
                     * $this->get($paramClassName,$params) 打印结果：
                     * -----------------------------------------------------------
                     * backend\components\EmailSenderBy163 Object
                     * (
                     *      [_name:backend\components\EmailSenderBy163:private] => Hello
                     * )
                     * -----------------------------------------------------------
                     * 经过get()之后，已经是返回的一个对象，这个对象就是User类中构造方法中参数指定的类EmailSenderBy163的实例化
                     */
                }
            }
        }

        /**
         * 反射注入参数
         */
        foreach ($params as $index => $param) {
            $dependencies[$index] = $param;
        }
        /**
         * 七、通过ReflectionClass的newInstanceArgs()方法根据给定的参数创建一个新的类实例
         * ==================================================
         *   转换前：
         *   Array
         *   (
         *      [EmailSenderBy163] => Hello
         *   )
         *
         *   ReflectionClass::newInstanceArgs()创建一个新的类实例后：
         *   Array
         *   (
         *   [0] => backend\components\EmailSenderBy163 Object
         *   (
         *      [_name:backend\components\EmailSenderBy163:private] => Hello
         *   )

         *   [EmailSenderBy163] => Hello
         *   )
         * ==================================================
         */
        return $reflection->newInstanceArgs($dependencies);
    }
}