<?php
namespace backend\components;

interface EmailSender
{
    public function send();
}