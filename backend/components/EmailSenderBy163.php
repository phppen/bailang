<?php

namespace backend\components;

use Yii;
use bailangzhan\Mailer;

class EmailSenderBy163 implements \backend\components\EmailSender
{
    // 保护属性，只能再本类中调用
    private $_name;

    public function __construct($name = '')
    {
        $this->_name = $name;
    }

    public function send()
    {
        $message = [
            'to' => '3003677848@qq.com',
            'subject' => '测试标题',
            'content' => $this->_name
        ];
        $mailer = new Mailer(Mailer::TYPE_1, $message);
        if ($mailer->sendMessage()) {
            return true;
        }
    }
}