<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();

echo "<?php\n";
?>

use yii\helpers\Html;
use <?= $generator->indexWidgetType === 'grid' ? "yii\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">
<?= $generator->enablePjax ? "    <?php Pjax::begin(); ?>\n" : '' ?>
<?php if(!empty($generator->searchModelClass)): ?>
<?= "    <?php " . ($generator->indexWidgetType === 'grid' ? "// " : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
<?php endif; ?>

    <p>
        <?= "<?= " ?>Html::a('<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;创建', ['create'], [
            'class' => 'btn btn-success',
            'id' => 'create',
            'data-toggle' => 'modal',
            'data-target' => '#operate-modal',
        ]) ?>
    </p>

<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=> '{items}<p class="text-right tooltip-demo">{pager}</p>',
        'pager'=>[
            //'options'=>['class'=>'hidden'] //关闭自带分页
            'firstPageLabel'=>"首页",
            'prevPageLabel'=>'上一页',
            'nextPageLabel'=>'下一页',
            'lastPageLabel'=>'尾页',
        ],
        // <?= !empty($generator->searchModelClass) ? "'filterModel' => \$searchModel,\n        'columns' => [\n" : "'columns' => [\n"; ?>
        // ['class' => 'yii\grid\SerialColumn'], // 不使用排序

<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        if (++$count < 6) {
            echo "            '" . $name . "',\n";
        } else {
            echo "            //'" . $name . "',\n";
        }
    }
} else {
    foreach ($tableSchema->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        if (++$count < 6) {
            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        } else {
            echo "            //'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        }
    }
}
?>

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'header' => '<i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;操作',
                'headerOptions'=> ['width'=> '152'],
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;信息', $url, [
                            'title' => '栏目信息',
                            'class' => 'btn btn-default btn-update',
                            'data-toggle' => 'modal',
                            'data-target' => '#operate-modal',
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;删除', $url, [
                            'title' => '删除',
                            'class' => 'btn btn-default',
                            'data' => [
                                'confirm' => '确定要删除吗?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>
<?= $generator->enablePjax ? "    <?php Pjax::end(); ?>\n" : '' ?>
</div>

<?= "<?php\n" ?>
/* Modal */
Modal::begin([
    'id' => 'operate-modal',
    'header' => '<h4 class="modal-title"></h4>',
]);
Modal::end();
$requestCreateUrl = Url::toRoute('create'); /* 创建 */
$requestUpdateUrl = Url::toRoute('update'); /* 更新 */
<?= "?>\n" ?>
<script type="text/javascript">
<?= "<?php" ?> $this->beginBlock('modal') <?= "?>\n" ?>
    /* 创建操作 */
    $('#create').on('click', function () {
        $('.modal-title').html('<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;创建');
        $.get('<?= "<?=" ?> $requestCreateUrl <?= "?>" ?>',
            function (data) {
                $('.modal-body').html(data);
                $('.modal-body').find('.btn-success').html('<i class="fa fa-check" aria-hidden="true"></i>&nbsp;创建');
            }
        );
    });
    /* 更新操作 */
    $('.btn-update').on('click', function () {
        $('.modal-title').html('信息');
        $.get('<?= "<?=" ?> $requestUpdateUrl <?= "?>" ?>', {id: $(this).closest('tr').data('key')},
            function (data) {
                $('.modal-body').html(data);
                $('.modal-body').find('.btn-success').html('<i class="fa fa-check" aria-hidden="true"></i>&nbsp;更新');
            }
        );
    });
<?= "<?php" ?> $this->endBlock(); <?= "?>\n" ?>
<?= "<?php" ?> $this->registerJs($this->blocks['modal'], \yii\web\View::POS_END); /*将编写的js代码注册到页面底部*/ <?= "?>\n" ?>
</script>
