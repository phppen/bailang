<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'aliases' => [
        '@mdm/admin' => '@vendor/mdmsoft/yii2-admin',
    ],
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
        ],
        /* Redactor 编辑器 */
        'redactor' => [
            'class' => 'backend\components\redactor\RedactorModule',
            'uploadDir' => '@imgserver/redactor', // @imgserver 在common/config/bootstrap里配置别名
            'uploadUrl' => 'http://img.bailang.com/redactor', // 图片服务器 URL
            'imageAllowExtensions'=>['jpg','png','gif','jpeg']
        ],
    ],
    // 'as myBehavior' => \backend\components\MyBehavior::className(),
    /* 命名行为，配置数组
    'as myBehavior2' => [
        'class' => \backend\components\MyBehavior::className(),
        'porp1' => '重写定义这个属性'
    ],*/
    /* 配置权限
    'as access' => [
        'class' => 'backend\components\AccessControl',
    ],
    */
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            //'*'
            'site/*'
        ]
    ],
    'components' => [
        'view' => [
            'theme' => [
                // 'basePath' => '@app/themes/spring',
                // 'baseUrl' => '@web/themes/spring',
                'pathMap' => [
                    '@app/views' => [
                        '@app/themes/default',
                    ]
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'backend\models\UserBackend',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
            'class' => 'yii\redis\Session'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /* 路由美化 */
        'urlManager' => [
            // 是否开启美化效果
            'enablePrettyUrl' => true,
            // 是否忽略脚本名index.php
            'showScriptName' => false,
            // 是否开启严格解析路由
            'enableStrictParsing' => false,
            // url后缀,示例'.html'
            'suffix' => '.html',
            'rules' => [
            ],
        ],

        'assetManager' => [
            'appendTimestamp' => true,
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-custom',
                ],
            ],
        ],
    ],
    'params' => $params,
];
