<?php

namespace backend\controllers;

use Yii;
use backend\models\Blog;
use backend\models\BlogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Componet;
use common\models\BlogCategory;
use yii\base\Exception;

// 图片上传
use common\components\Upload as NewUpload;
use yii\web\Response;

/**
 * BlogController implements the CRUD actions for Blog model.
 */
class BlogController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            //附加行为
            // 'myBehavior' => \backend\components\MyBehavior::className(),
            // 命名方式附加行为到当前控制器
            /*
            'as access' => [
                'class' => 'backend\components\AccessControl',
            ],*/
            // VerbFilter检查请求动作的HTTP请求方式是否允许执行， 如果不允许，会抛出HTTP 405异常
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Blog models.
     * @return mixed
     */
    public function actionIndex()
    {

        //$be = $this->getBehavior('myBehavior');
        //var_dump($be);
        //echo Yii::$app->getBehavior('myBehavior2')->test();

        if (!Yii::$app->user->can('/blog/index')) {
            throw new \yii\web\ForbiddenHttpException("You没权限访问.");
        }

        $searchModel = new BlogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blog model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Blog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Blog();
        $model->created_at = time();
        $model->update_at = time();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {

                $model->save(false);
                // 这里是获取刚刚插入blog表的ID
                $blogId = $model->id;
                $data = [];
                foreach ($model->category as $k => $v) {
                    $data[] = [$blogId, $v];
                }
                // 获取blogCategory模型的所有属性和表名
                $blogCategory = new BlogCategory();
                $attributes = ['blog_id', 'category_id'];
                $tableName = $blogCategory::tableName();
                $db = BlogCategory::getDb();
                // 批量插入栏目到BlogCategory::table表
                $db->createCommand()->batchInsert(
                    $tableName,
                    $attributes,
                    $data
                )->execute();
                // 提交
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', '发布成功！');
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                // 回滚
                $transaction->rollBack();
                // 抛出异常
                throw $e;
            }
        } else {
            // var_dump($model->errors);
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Blog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->update_at = time();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $transation = Yii::$app->db->beginTransaction();
            try {
                $model->save(false);
                $blogId = $model->id;

                $data = [];
                foreach ($model->category as $k => $v) {
                    $data[] = [$blogId, $v];
                }
                $blogCategory = new BlogCategory();
                $attributes = ['blog_id', 'category_id'];
                $tableName = $blogCategory::tableName();
                $db = BlogCategory::getDb();
                $sql = "DELETE FROM `{$tableName}` WHERE `blog_id` = :bid";
                $db->createCommand($sql, ['bid' => $id])->execute();

                $db->createCommand()->batchInsert(
                    $tableName,
                    $attributes,
                    $data
                )->execute();
                // 提交
                $transation->commit();
                Yii::$app->getSession()->setFlash('success', '更新成功！');
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                $transation->rollBack();
                throw $e;
            }
        } else {
            // 获取关联，为category赋值
            $model->category = BlogCategory::getRelationCategorys($id);
            // var_dump($model->category);exit();
            return $this->render('update', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Deletes an existing Blog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Blog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blog::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * 图片上传方法
     */
    public function actionUpload()
    {
        try {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model = new NewUpload();
            $info = $model->upImage();

            if ($info && is_array($info)) {
                return $info;
            } else {
                return ['code' => 1, 'msg' => 'error'];
            }

        } catch (\Exception $e) {
            return ['code' => 1, 'msg' => $e->getMessage()];
        }
    }
}
