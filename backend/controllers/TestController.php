<?php

namespace backend\controllers;

use Yii;
use backend\models\Test;
use backend\models\TestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use bailangzhan\Mailer;
use backend\components\Student;

// 测试Redis
use backend\models\RedisCustomer;
/**
 * TestController implements the CRUD actions for Test model.
 */
class TestController extends Controller implements \backend\components\EmailSender
{
    public $_mailer;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function getClassSource(\ReflectionClass $class)
    {
        $path = $class->getFileName(); // 获取文件的绝对路径
        $lines = @file($path); // 获的由文件中所有行组成的数组
        $from = $class->getStartLine(); // 提供类的起始行
        $to = $class->getEndLine(); // 提供类的终止行
        $len = $to - $from + 1;
        // array_slice 函数在数组中根据条件取出一段值，并返回。
        // implode 函数返回由数组元素组合成的字符串。
        return implode(array_slice($lines, $from -1, $len));
    }

    /**
     * Lists all Test models.
     * @return mixed
     */
    public function actionIndex()
    {
        // 实例化 IoC 容器
        $container = new \backend\components\Container;

        $user = $container->get('\backend\components\User', ['EmailSenderBy163' => 'Hello']);
        echo '<pre>';
        print_r($user);

        /** 先屏蔽掉发送邮件
        $emailCon = '发送第N封测试邮件NO.1026';
        if ($emailSenderBy163->send()) {
            echo 'Success!'.$emailCon.'-(发送成功)';
        } else {
            echo 'Error!'.$emailCon.'-(发送失败)';
        }
        */
    }



    public function classData(\ReflectionClass $class) {
        $details = '';
        $name = $class->getName();          // 返回要检查的类名
        if ($class->isUserDefined()) {      // 检查类是否由用户定义
            $details .= "$name is user defined" . PHP_EOL;
        }
        if ($class->isInternal()) {         // 检查类是否由扩展或核心在内部定义
            $details .= "$name is built-in" . PHP_EOL;
        }
        if ($class->isInterface()) {        // 检查类是否是一个接口
            $details .= "$name is interface" . PHP_EOL;
        }
        if ($class->isAbstract()) {         // 检查类是否是抽象类
            $details .= "$name is an abstract class" . PHP_EOL;
        }
        if ($class->isFinal()) {            // 检查类是否声明为 final
            $details .= "$name is a final class" . PHP_EOL;
        }
        if ($class->isInstantiable()) {     // 检查类是否可实例化
            $details .= "$name can be instantiated" . PHP_EOL;
        } else {
            $details .= "$name can not be instantiated" . PHP_EOL;
        }
        return $details;
    }

    /**
     * Displays a single Test model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Test model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Test();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->getFlash('success', '添加成功!');
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * 异步校验表单模型
     */
    public function actionValidateForm()
    {
        $model = new Test();
        $model->load(Yii::$app->request->post());
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return \yii\widgets\ActiveForm::validate($model);
    }

    /**
     * Updates an existing Test model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->getFlash('success', '更新成功!');
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Test model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->getFlash('success', '删除成功!');
        return $this->redirect(['index']);
    }

    public function actionTestArray()
    {
        // $db = new \common\models\Category();
        // $arr = $db::find(['id'=>'id', 'name'=>'name'])->all();
        $arr = [
            ['id' => 1, 'name' => '分类1'],
            ['id' => 3, 'name' => '分类2'],
            ['id' => 5, 'name' => '分类3'],
        ];
        $arrs = ArrayHelper::map($arr, 'id', 'name');
        print_r($arrs);
    }

    /**
     * Finds the Test model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Test the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Test::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function send()
    {
        $this->_mailer->sendMessage();
        return true;
    }
}
