<?php
namespace backend\controllers;
use backend\components\event\MailEvent;
use Yii;
use yii\web\Controller;

/**
 * 事件测试
 */
class EventTestController extends Controller
{
    // 定义事件
    const TEST_EVENT = 'test_event';

    public function init()
    {
        parent::init();
        // 绑定事件
        $this->on(self::TEST_EVENT, ['backend\components\TestEvent', 'myEvent']);
    }

    public function actionIndex ()
    {
        $event = new MailEvent();
        $event->email = '39666@qq.com';
        $this->trigger(self::TEST_EVENT, $event);
    }


}
?>