<?php
namespace backend\controllers;
use Yii;
use yii\web\Controller;

class RbacController extends Controller
{
    public function actionInit ()
    {
        $auth = Yii::$app->authManager;

        // 创建'/blog/index'权限
        $blogIndex = $auth->createPermission('/blog/index');
        $blogIndex->description = '博客列表';
        $auth->add($blogIndex);

        // 创建一个'博客管理'的角色
        $blogManage = $auth->createRole('博客管理');
        $auth->add($blogManage);

        // 为'博客管理'角色分配'/blog/index'权限
        $auth->addChild($blogManage, $blogIndex);

        // 为id=1的用户分配角色
        $auth->assign($blogManage, 1);
    }

    public function actionInit2 ()
    {
        $auth = Yii::$app->authManager;

        // 创建权限
        $blogView = $auth->createPermission('/blog/view');
        $blogView->description = '博客正文';
        $auth->add($blogView);

        $blogCreate = $auth->createPermission('/blog/create');
        $blogCreate->description = '创建博客';
        $auth->add($blogCreate);

        $blogUpdate = $auth->createPermission('/blog/update');
        $blogUpdate->description = '更新博客';
        $auth->add($blogUpdate);

        $blogDelete = $auth->createPermission('/blog/delete');
        $blogDelete->description = '删除博客';
        $auth->add($blogDelete);

        // 分配权限
        $blogManage = $auth->getRole('博客管理');
        $auth->addChild($blogManage, $blogView);
        $auth->addChild($blogManage, $blogCreate);
        $auth->addChild($blogManage, $blogUpdate);
        $auth->addChild($blogManage, $blogDelete);
    }
}
?>