<?php

namespace backend\assets;

use yii\web\AssetBundle;

class TestAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/upload';
    // public $basePath = '@webroot'; // 指向@app/web物理路径目录
    // public $baseUrl = '@web'; // 指向@app/web URL路径
    public $css = [
        //'css/site_test.css',
        'css/add.css',
    ];
}
