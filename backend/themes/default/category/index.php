<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '分类列表';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('创建栏目', ['create'], [
            'class' => 'btn btn-success',
            'id' => 'create',
            'data-toggle' => 'modal', // Modal固定写法
            'data-target' => '#operate-modal', // 同Modal::begin中的ID
        ]) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout'=> '{items}<p class="text-right tooltip-demo">{pager}</p>',
        'pager'=>[
            //'options'=>['class'=>'hidden']//关闭自带分页
            'firstPageLabel'=>"首页",
            'prevPageLabel'=>'上一页',
            'nextPageLabel'=>'下一页',
            'lastPageLabel'=>'尾页',
        ],
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'], 关闭#
            'id',
            'name',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'header' => '操作',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a('栏目信息', $url, [
                            'title' => '栏目信息',
                            'class' => 'btn btn-default btn-update',
                            'data-toggle' => 'modal',
                            'data-target' => '#operate-modal',
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('删除', $url, [
                            'title' => '删除',
                            'class' => 'btn btn-default',
                            'data' => [
                                'confirm' => '确定要删除么？',
                                'method' => 'post',
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>

<?php
use yii\bootstrap\Modal;
Modal::begin([
    'id' => 'operate-modal',
    'header' => '<h4 class="modal-title"></h4>',
]);
Modal::end();
?>

<?php
use yii\helpers\Url;
$requestCreateUrl = Url::toRoute('create');
$requestUpdateUrl = Url::toRoute('update');
?>
<script type="text/javascript">
<?php $this->beginBlock('category') ?>
    // 创建栏目
    $('#create').on('click', function () {
        $('.modal-title').html('创建栏目');
        $.get('<?= $requestCreateUrl ?>',
            function (data) {
                $('.modal-body').html(data);
            }
        );
    });
    // 更新栏目
    $('.btn-update').on('click', function () {
        $('.modal-title').html('更新栏目');
        $.get('<?= $requestUpdateUrl ?>', {id: $(this).closest('tr').data('key')},
            function (data) {
                $('.modal-body').html(data);
            }
        );
    });
<?php $this->endBlock(); ?>
<?php $this->registerJs($this->blocks['category'],\yii\web\View::POS_END); //将编写的js代码注册到页面底部 ?>
</script>
