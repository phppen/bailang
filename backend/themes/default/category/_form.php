<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

// ajax 异步验证
$validationUrl = ['validate-form']; // 对应的路由Url
if (!$model->isNewRecord) {
    $validationUrl['id'] = $model->id; // 参数
}
?>
<div class="category-form">
    <?php $form = ActiveForm::begin([
        'id' => 'category-form', // ID自定义,必须
        'enableAjaxValidation' => true, // enableAjaxValidation必须
        'validationUrl' => $validationUrl, // 对应的路由
    ]); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
