<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Category;
use backend\models\Blog;
use yii\redactor\widgets\Redactor;
?>
<div class="blog-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'thumb_img')->widget('manks\FileInput', []) ?>
    <?= $form->field($model, 'content')->widget(Redactor::className(), [
        'clientOptions' => [
            'lang' => 'zh_cn', 'minHeight' => 400,
            'plugins' => ['clips', 'fontcolor','imagemanager']
        ]
    ]) ?>

    <?= $form->field($model, 'is_delete')->dropDownList(Blog::dropDownList('is_delete')) ?>
    <?php // $model->category = [2,3,4]; ?>
    <?= $form->field($model, 'category')->label('栏目')->checkboxList(Category::dropDownList()) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
