<?php
/**
 * Redis 测试模型
 */
namespace backend\models;

use Yii;
use yii\redis\ActiveRecord;
use backend\models\Blog;

class RedisCustomer extends ActiveRecord
{
    /**
     * 主键 设置为 rid
     */
    public static function primaryKey()
    {
        return ['rid'];
    }

    /**
     * 模型对应记录的属性列表
     */
    public function attributes()
    {
        return ['rid', 'title', 'thumb_img', 'content', 'views', 'is_delete', 'created_at', 'update_at'];
    }

    /**
     * 定义和 Blog 模型的关系
     */
    public function getBlogTable()
    {
        return $this->hasMany(Blog::className(), ['id' => 'rid'])->asArray()->all();
    }
}