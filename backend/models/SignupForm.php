<?php
namespace backend\models;

use yii\base\Model;
use backend\models\UserBackend;

class SignupForm extends Model
{
    // 设置属性
    public $username;
    public $email;
    public $password;
    public $created_at;
    public $updated_at;

    /**
     * 对数据的校验规则
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'filter', 'filter' => 'trim'],
            [['username', 'email', 'password'], 'required', 'message' => '此项不能为空'],
            [['username', 'email'], 'unique', 'targetClass' => '\backend\models\UserBackend', 'message' => '此项已经存在'],
            [['username', 'email'], 'string', 'min' => 2, 'max' => 255],
            ['email', 'email'],
            ['password', 'string', 'min' => 6, 'tooShort' => '密码至少填写6位'],
            [['created_at', 'updated_at'], 'default', 'value' => time()]
        ];
    }

    /**
     * @inheritdoc
     */
    public function signup()
    {
        /**
         * 调用validate方法进行验证
         * 验证规则就是上面的rules方法
         */
        if (!$this->validate()) {
            return false;
        }
        // 实现入库操作
        $user = new UserBackend();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->created_at = $this->created_at;
        $user->updated_at = $this->updated_at;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        // save(false)的意思是：不调用UserBackend的rules再做校验并实现数据入库操作
        return $user->save(false);
    }
}
?>