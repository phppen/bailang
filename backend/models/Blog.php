<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property int $id
 * @property string $title 标题
 * @property string $thumb_img 缩率图
 * @property string $content 内容
 * @property int $views 点击量
 * @property int $is_delete 是否删除1为未删除2已删除
 * @property int $created_at 添加时间
 * @property int $update_at 更新时间
 */
class Blog extends \yii\db\ActiveRecord
{
    public $category;

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_BEFORE_INSERT,[$this, 'onBeforeInsert']);
        $this->on(self::EVENT_AFTER_INSERT,[$this, 'onAfterInsert']);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'created_at', 'update_at', 'category'], 'required'],
            [['content'], 'string'],
            [['views', 'is_delete', 'created_at', 'update_at'], 'integer'],
            [['title'], 'string', 'max' => 100],

            [['thumb_img'], 'required'],
            [['thumb_img'], 'safe'],
            [['thumb_img'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '标题',
            'thumb_img' => '缩略图',
            'content' => '内容',
            'views' => '点击量',
            'is_delete' => '是否删除1为未删除2已删除',
            'created_at' => '添加时间',
            'update_at' => '更新时间',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function dropDownList()
    {
        return [
            '1' => '删除',
            '2' => '不删除'
        ];
    }

    /**
     * 事件回调函数
     */
    public function onBeforeInsert ($event)
    {
        yii::info('这是绑定插入之前的事件.');
    }
    public function onAfterInsert ($event)
    {
        yii::info('这是绑定插入之后的事件.');
    }
}
