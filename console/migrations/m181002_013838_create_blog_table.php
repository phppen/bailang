<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog`.
 */
class m181002_013838_create_blog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('blog', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull()->comment('标题'),
            'content' => $this->text()->notNull()->comment('内容'),
            'views' => $this->integer()->notNull()->defaultValue('0')->comment('点击量'),
            'is_delete' => $this->tinyInteger(4)->notNull()->defaultValue('1')->comment('是否删除1为未删除2已删除'),
            'created_at' => $this->bigInteger()->notNull()->comment('添加时间'),
            'update_at' => $this->bigInteger()->notNull()->comment('更新时间')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('blog');
    }
}
