<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * Test Console Application ^_^
 */
class TestController extends Controller
{
    public function actionIndex ($name, $age)
    {
        echo "Name is: {$name}.\n";
        echo "Age is: {$age}.\n";
        echo "Thank you!\n";
    }

    public function actionFozu ()
    {
        echo "                             _ooOoo_\n";
        echo "                            o8888888o\n";
        echo "                            88\" . \"88\n";
        echo "                            (| -_- |)\n";
        echo "                             O\\ = /O\n";
        echo "                         ____/`---'\\____\n";
        echo "                         . '  \\| |// `. \\\n";
        echo "                        / \\|||  : |||// \\\n";
        echo "                      / _||||| -:- |||||- \\\n";
        echo "                        | | \\\\ - /// | |\n";
        echo "                      | \\_| ''\\---/'' | |\n";
        echo "                       \\ .-\\__ `-` ___/-. /\n";
        echo "                    ___`. .' /--.--\\ `. . __\n";
        echo "                 .\"\\\" '< `.___\\_<|>_/___.' >'\"\".\n";
        echo "                | | : `- \\`.;`\\ _ /`;.`/ - ` : | |\n";
        echo "                  \\ \\ `-. \\_ __\\ /__ _/ .-` / /\n";
        echo "          ======`-.____`-.___\\_____/___.-`____.-'======\n";
        echo "                             `=---='\n";
        echo " \n";
        echo "          ------------------------------------------------\n";
        echo "                   佛祖保佑             永无BUG\n";
        echo "          ------------------------------------------------\n";
        echo "                  写字楼里写字间，写字间中程序员； \n";
        echo "                  程序人员写程序，又将程序换酒钱； \n";
        echo "                  酒醒只在屏前坐，酒醉还来屏下眠； \n";
        echo "                  酒醉酒醒日复日，屏前屏下年复年； \n";
        echo "                  但愿老死电脑间，不愿鞠躬老板前； \n";
        echo "                  奔驰宝马贵者趣，公交自行程序员； \n";
        echo "                  别人笑我太疯癫，我笑自己命太贱； \n";
        echo "                  但见满街漂亮妹，哪个归得程序员。\n";
    }
}