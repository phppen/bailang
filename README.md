<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 学习项目</h1>
    <br>
</p>

### 安装
```
// 加载依赖
composer update
// 初始化
./init
```
### 安装数据库
```
./yii migrate
./yii migrate --migrationPath=@yii/rbac/migrations/
./yii migrate/to m140602_111327_create_menu_table.php  --migrationPath=@mdm/admin/migrations
```
### backend\config\main-local.php 配置
```
<?php
$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'wwIEcXzWbQcmgd1AkZl9XRIM6n9PnIKi',
        ],
    ],
];
if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*.*.*.*'],
        'generators' => [
            'crud' => [ //生成器名称
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [ //设置我们自己的模板
                    //模板名 => 模板路径
                    'myCrud' => '@app/components/gii-custom/crud/default',
                ]
            ]
        ],
    ];
}
return $config;
```