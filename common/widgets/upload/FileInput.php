<?php
namespace common\widgets\upload;

use Yii;
use yii\widgets\InputWidget;
use yii\helpers\Html;
use yii\base\InvalidConfigException;

/**
 * 图片上传插件
 */
class FileInput extends InputWidget
{
    public $clientOptions = [];
    public function run ()
    {
        // 注册客户端所需要的资源
        $this->registerClientScript();
        // 构建html结构
        if ($this->hasModel()) {
            $this->options = array_merge($this->options, $this->clientOptions);
            $file = Html::activeInput('file', $this->model, $this->attribute, $this->options);
            // 如果当前模型有该属性值，则默认显示
            if ($image = $this->model->{str_replace(['[',']'], '', $this->attribute)}) {
                $li = Html::tag('li', '', ['class' => 'uploader__file', 'style' => 'background: url(' . Yii::$app->params['imageServer'] . $image . ') no-repeat; background-size: 100%;']);
                // 追加一个隐藏的input框，否则update的时候回覆盖原图片
                $file .= Html::activeInput('hidden', $this->model, $this->abbribute, ['value' => $image]);
            }
            $uploaderFiles = Html::tag('ul', isset($li) ? $li : '', ['class' => 'uploaderFiles']);
            $inputButton = Html::tag('div', $file, ['class' => 'input-box']);
            echo Html::tag('div', $uploaderFiles.$inputButton, ['class' => 'file-div']);
        } else {
            throw new InvalidConfigException("'model' must be specified.");
        }
    }
    /**
     * Register the needed client script and options.
     */
    public function registerClientScript ()
    {
        $view = $this->getView();
        FileInputAsset::register($view);
    }
}