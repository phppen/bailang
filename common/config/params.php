<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'imageServer' => 'http://img.bailang.com',
    'domain' => 'http://img.bailang.com/',
    'webuploader' => [
        // 后端处理图片的地址，value 是相对的地址
        'uploadUrl' => 'blog/upload',
        // 多文件分隔符
        'delimiter' => ',',
        // 基本配置
        'baseConfig' => [
            'defaultImage' => 'http://img.bailang.com/uploads/default.jpg',
            'disableGlobalDnd' => true,
            'accept' => [
                'title' => 'Images',
                'extensions' => 'gif,jpg,jpeg,bmp,png',
                'mimeTypes' => 'image/*',
            ],
            'pick' => [
                'multiple' => false,
            ],
        ],
    ],
    // http://img.bailang.com/uploads/
    'imageUploadRelativePath' => './imgserver/uploads/', // 图片默认上传的目录
    'imageUploadSuccessPath' => 'uploads/', // 图片上传成功后，路径前缀
];
